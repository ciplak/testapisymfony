<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUsers($manager);
    }

    private function loadUsers(ObjectManager $manager): void
    {
        foreach ($this->getUserData() as [$username, $password, $email, $roles]) {
            $user = new User($username);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));

            $manager->persist($user);
            $this->addReference($username, $user);
        }

        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            // $userData = [$username, $password, $email, $roles];
            ['customer1', 'customer1', 'customer1@symfony.com', ['ROLE_ADMIN']],
            ['customer2', 'customer2', 'customer2@symfony.com', ['ROLE_ADMIN']],
            ['customer3', 'customer3', 'customer3@symfony.com', ['ROLE_ADMIN']],
        ];
    }
}
