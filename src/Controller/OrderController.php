<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    private $orderRepository;
    /**
     * OrderController constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Create
     *
     * @Route(name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $orderCode = $request->get('orderCode');
        $productId = $request->get('productId');
        $quantity = $request->get('quantity');
        $address = $request->get('address');
        $shippingDate = $request->get('shippingDate');

        $order = new Order();
        $order->setOrderCode($orderCode);
        $order->setProductId($productId);
        $order->setQuantity($quantity);
        $order->setAddress($address);
        $order->setShippingDate(date($shippingDate));
        $em->persist($order);
        $em->flush();

        return new Response('Created New Order', 201);
    }

    /**
     * Update
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $orderId = (int)$request->get('orderId');
        $orderCode = $request->get('orderCode');
        $productId = $request->get('productId');
        $quantity = $request->get('quantity');
        $address = $request->get('address');
        $shippingDate = $request->get('shippingDate');

        $order = $this->getDoctrine()->getRepository(Order::class)->find($orderId);

        if($order->getShippingDate() < $shippingDate){
            $order->setOrderCode($orderCode);
            $order->setProductId($productId);
            $order->setQuantity($quantity);
            $order->setAddress($address);
            $order->setShippingDate(date($shippingDate));

            $em->persist($order);
            $em->flush();

            return new Response('Order Updated', 200); // TODO 204
        } else {
            return new Response('Shipping Date Expired', 200);
        }
    }

    /**
     * Detail
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detail(Request $request)
    {
        $orderId = (int)$request->get('orderId');
        $repository = $this->getDoctrine()->getRepository(Order::class);
        $order = $repository->find($orderId);

        $serializer = new Serializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );

        $data = $serializer->serialize($order, 'json');

        return new JsonResponse(['data' => json_decode($data)]);
    }

    /**
     * List
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Order::class);
        $order = $repository->findAll();

        $serializer = new Serializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );

        $data = $serializer->serialize($order, 'json');

        return new JsonResponse(['data' => json_decode($data)]);
    }
}
